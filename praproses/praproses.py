import arcpy
from arcpy.sa import *
from arcpy import env
import numpy as np
import sys, os, string, glob
import time
from tqdm import tqdm

'''
	Convert raster to array
	Requirements:
		- path : containts location of raster
	
	Variables:
		- tcorr : containts file raster
		- LowerLeft : containts point lower left raster's coordinate
		- tcorr_arr : containts raster in array
'''
def RasterToArray(path):
	tcorr = arcpy.Raster(r""+path)
	lowerLeft = arcpy.Point(tcorr.extent.XMin,tcorr.extent.YMin)
	#Convert Raster to Numpy Array
	#Raster is 7951 columns x 6971 rows, Floating point 32 bit, 211MB in size
	tcorr_arr = arcpy.RasterToNumPyArray(tcorr,lowerLeft,7951,6971,nodata_to_value=0)
	del tcorr
	return tcorr_arr


'''
	Calculate EVI
	Requirements:
		- r : containts array raster of channel red
		- g : containts array raster of channel green
		- b : containts array raster of channel blue
		- nir : containts array raster of channel near infrared
		- n : containts the number of saved files division
		- path : containts location of saved files
'''
def EVI(r, g, b, nir, n, path):
	x = len(r)
	y = len(r[0])
	maxr = 1.0*r.max()
	maxg = 1.0*g.max()
	maxb = 1.0*b.max()
	maxnir = 1.0*nir.max()
	
	for count in range (n):
		out = open(path+'/evi_'+str(count)+'.txt', 'w')
		for i in tqdm(range (x*count/n, (x*(count+1))/n)):
			for j in range (y):
				if b[i][j]/maxb > r[i][j]/maxr or r[i][j]/maxr > nir[i][j]/maxnir:
					evi = 1.5* (nir[i][j]/maxnir - r[i][j]/maxr) / ( 0.5 + nir[i][j]/maxnir + r[i][j]/maxr)
				else:
					evi = 2.5* (nir[i][j]/maxnir - r[i][j]/maxr) / ( 1 + nir[i][j]/maxnir + 6*r[i][j]/maxr - 7.5*b[i][j]/maxb)
				out.write(str(evi))
				out.write('\n')
		out.close()
	del r, g, b, nir

def EVI8(path, n):
	for count in range (n):
		evi = open(path+'/evi_'+str(count)+'.txt', 'r').read().split('\n')
		evi8 = open(path+'/evi8_'+str(count)+'.txt', 'w')
		for i in tqdm(evi):
			if i != '':
				evi8.write(str(125+125*eval(i))+'\n')
		del evi
		evi8.close()
