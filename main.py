import arcpy
from arcpy.sa import *
from arcpy import env
import numpy as np
import sys, os, string, glob
import time
from praproses import *
from tqdm import tqdm

def writeAllRaster(r, g, b, nir, n, path):
	x = len(r)
	y = len(r[0])
	for count in range(n):
		out = open(path+'_'+str(count)+'.txt', 'w')
		for i in tqdm(range (x*count/n, (x*(count+1))/n)):
			for j in range (y):
				out.write(str(r[i][j])+' '+str(g[i][j])+' '+str(b[i][j])+' '+str(nir[i][j]))
				out.write('\n')
		out.close()	
	del r, g, b, nir

def writeAllNormRaster(r, g, b, nir, n, path):
	x = len(r)
	y = len(r[0])
	maxr = 1.0*r.max()
	maxg = 1.0*g.max()
	maxb = 1.0*b.max()
	maxnir = 1.0*nir.max()
	
	for count in range (n):
		out = open(path+'_N'+str(count)+'.txt', 'w')
		for i in tqdm(range (x*count/n, (x*(count+1))/n)):
			for j in range (y):
				out.write(str(r[i][j]/maxr)+' '+str(g[i][j]/maxg)+' '+str(b[i][j]/maxb)+' '+str(nir[i][j]/maxnir))
				out.write('\n')
		out.close()
	del r, g, b, nir


def printRaster(raster):
	for i in tqdm(raster):
		for j in i:
			print(str(j))

def normal(arr):
	norm = []
	n_max = arr.max()*1.0
	for i in tqdm(arr):
		norm_row = []
		for j in i:
			norm_row.append(j/n_max)
		norm.append(norm_row)
		del norm_row
	return norm


def wheatPhase(PathEvi, PathEviL, n):
	for counter in range(n):
		evi = open(PathEvi+'/evi_'+str(counter)+'.txt', 'r').read().split('\n')
		eviL = open(PathEviL+'/evi_'+str(counter)+'.txt', 'r').read().split('\n')
		wheatPhase = open('E:/WheatPhase/output/WheatPhase/hasil_'+str(counter)+'.txt', 'w')
		for i in tqdm(range(0, len(evi)-1)):
			dEVI = eval(evi[i])-eval(eviL[i])
			if(eval(evi[i]) <= 0.10):
				wheatPhase.write('Air\n')
			elif(eval(evi[i]) > 0.10 and eval(evi[i]) < 0.22):
				wheatPhase.write('Bera\n')
			elif(dEVI > 0):
				wheatPhase.write('Vegetatif\n')
			elif(dEVI < 0):
				wheatPhase.write('Generatif\n')
			else:
				wheatPhase.write('NULL\n')
		del evi, eviL

#Set Environment
arcpy.CheckOutExtension("spatial")
arcpy.env.overwriteOutput = True
env.extent = "MINOF"
Coordsystem = "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]"

# #set input data
# red = RasterToArray('E:/DATA LAPAN/KALIMANTANBARAT/LC81210602015188RPI00/LC81210602015188RPI00_B2.TIF')
# green = RasterToArray('E:/DATA LAPAN/KALIMANTANBARAT/LC81210602015188RPI00/LC81210602015188RPI00_B3.TIF')
# blue = RasterToArray('E:/DATA LAPAN/KALIMANTANBARAT/LC81210602015188RPI00/LC81210602015188RPI00_B4.TIF')
# nir = RasterToArray('E:/DATA LAPAN/KALIMANTANBARAT/LC81210602015188RPI00/LC81210602015188RPI00_B5.TIF')

# # print('writing..')
# # writeAllRaster(red, green, blue, nir, 10, 'E:/WheatPhase/output/LC81210602015188RPI00/raster')
# # writeAllNormRaster(red, green, blue, nir, 10, 'E:/WheatPhase/output/LC81210602015188RPI00/raster')
# print('Menghitung EVI...')
# EVI(red, green, blue, nir, 10, 'E:/WheatPhase/output/LC81210602015188RPI00')
# del red, green, blue, nir

# print('Menghitung EVI8...')
# EVI8('E:/WheatPhase/output/LC81210602015188RPI00', 10)


# red = RasterToArray('E:/DATA LAPAN/KALIMANTANBARAT/LC81220592015275RPI00/LC81220592015275RPI00_B2.TIF')
# green = RasterToArray('E:/DATA LAPAN\KALIMANTANBARAT/LC81220592015275RPI00/LC81220592015275RPI00_B3.TIF')
# blue = RasterToArray('E:/DATA LAPAN\KALIMANTANBARAT/LC81220592015275RPI00/LC81220592015275RPI00_B4.TIF')
# nir = RasterToArray('E:/DATA LAPAN\KALIMANTANBARAT/LC81220592015275RPI00/LC81220592015275RPI00_B5.TIF')

# print('Menghitung EVI...')
# EVI(red, green, blue, nir, 10, 'E:/WheatPhase/output/LC81220592015275RPI00')
# del red, green, blue, nir

# print('Menghitung EVI8...')
# EVI8('E:/WheatPhase/output/LC81220592015275RPI00', 10)

wheatPhase('E:/WheatPhase/output/LC81210602015188RPI00','E:/WheatPhase/output/LC81220592015275RPI00', 10)


# norm = red/(red.max()*1.0)

#menghitung EVI
# evi = EVI(red, blue, nir)
# print()
# evi8 = EVI8(evi)
# print()

# write(norm)



